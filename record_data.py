#!/usr/bin/env python3
from argparse import ArgumentParser
import os

def main():
    parser = ArgumentParser(description="Record results of coin tossing")
    parser.add_argument("output", help="Output file name (csv)")
    parser.add_argument("--coins-per-throw", default=4, help="How many coins will be tossed per throw")
    parser.add_argument("-f", action="store_true", help="Force overwrite of existing output file")
    args = parser.parse_args()

    output_fn = args.output
    cpt = args.coins_per_throw
    force_overwrite = args.f

    if os.path.exists(output_fn) and not force_overwrite:
        print("Output file '{}' already exists. Use -f to force overwriting")
        exit(1)

    with open(output_fn, "w") as out:
        out.write("throw,coin,side\n")
        print("Enter {} characters (X, O or E) in order of coin numbers. Enter to store. Ctrl+D to quit.".format(cpt))
        throw_no = 0
        while(True):
            try:
                inp = input("Throw {0} (4x X/O/E): ".format(throw_no)).upper()
            except EOFError:
                print("\nThanks for tossing!")
                break

            if len(inp) != cpt:
                print("Wrong number of characters, please try again")
                continue

            valid_chars = set("XOE")
            inp_chars = set(inp)
            if not inp_chars.issubset(valid_chars):
                invalid_chars = inp_chars - valid_chars
                print("Invalid character{} '{}', please try again".format("s" if len(invalid_chars) > 1 else "", ", ".join(invalid_chars)))
                continue

            for coin_no, res_char in enumerate(inp):
                out.write("{},{},{}\n".format(throw_no, coin_no, res_char))

            throw_no += 1

if __name__ == "__main__":
    main()
