# Results

## Run 1

Date: 2018-02-10
Surface: short-pile 'soft' carpet tiles
Drop height: 750mm
Coins used: 1, 2, 3, 4
Average coin diameter: 18.6mm ## TODO, check this
Average coin thickness: 9.45mm ### TODO, check this
D/T ratio: 1.97 (target: 2)

Some of the coins have slightly convex faces (approximately 0.2mm of protrusion).

## Run 2

Date: 2018-02-10

Repeat of Run 1.
