#!/usr/bin/env python3
from argparse import ArgumentParser
import os

def main():
    parser = ArgumentParser(description="Analyse results of coin tossing")
    parser.add_argument("input", nargs="+", help="Input file name(s). Multiple files are combined and treated as a single dataset.")
    args = parser.parse_args()

    input_fns = args.input

    counts = {'X': 0, 'O': 0, 'E': 0}

    for input_fn in input_fns:
        print("Reading results from {}".format(input_fn))
        with open(input_fn) as inp:
            inp.readline() # Skip header
            for line in inp:
                throw_no,coin_no,side = line.strip().split(",")
                counts[side] += 1

    total_tosses = sum(counts.values())
    print("Total number of tosses: {}".format(total_tosses))
    print("X: {} ({}%)".format(counts['X'], 100 * counts['X'] / total_tosses))
    print("O: {} ({}%)".format(counts['O'], 100 * counts['O'] / total_tosses))
    print("E: {} ({}%)".format(counts['E'], 100 * counts['E'] / total_tosses))


if __name__ == "__main__":
    main()
