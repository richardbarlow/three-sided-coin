# Three Sided Coin

This is a collection of tools to aid with data collection and processing of tossing three sided coins. A three sided coin is one that has an equal probability of landing on either face or its edge (1/3 probability of each).

## Recoding a Tossing Session

Run `./record_data.py NAME.csv`. The results will be written to `NAME.csv`. You can also specify how many coins you are tossing in each throw with the `--coins-per-throw` argument (e.g. `./record_data.py foo.csv --coins-per-throw 10`). It currently defaults to 4 because I have made 4 coins.
